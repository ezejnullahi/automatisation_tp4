package iut.automatisation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainAlgosTest {

    @Test
    void testCalculerSomme() {
        assertEquals(15, MainAlgos.calculerSomme(5));
        assertEquals(55, MainAlgos.calculerSomme(10));
        assertEquals(0, MainAlgos.calculerSomme(0));
        assertEquals(1, MainAlgos.calculerSomme(1));
    }

    @Test
    void testEstPalindrome() {
        assertTrue(MainAlgos.estPalindrome("radar"));
        assertTrue(MainAlgos.estPalindrome("level"));
        assertFalse(MainAlgos.estPalindrome("hello"));
        assertTrue(MainAlgos.estPalindrome("a"));
    }

    @Test
    void testTrierTableau() {
        int[] tableau1 = {5, 2, 9, 1, 5, 6};
        MainAlgos.trierTableau(tableau1);
        assertArrayEquals(new int[]{1, 2, 5, 5, 6, 9}, tableau1);

        int[] tableau2 = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};
        MainAlgos.trierTableau(tableau2);
        assertArrayEquals(new int[]{1, 1, 2, 3, 3, 4, 5, 5, 5, 6, 9}, tableau2);

        int[] tableau3 = {5};
        MainAlgos.trierTableau(tableau3);
        assertArrayEquals(new int[]{5}, tableau3);
    }
}
