package iut.automatisation;

import java.util.Arrays;

public class MainAlgos {
    public static void main(String[] args) {
        // 1. Algorithme de Somme des N premiers entiers
        
        int n = 5;
        int somme = calculerSomme(n);
        System.out.println("La somme des " + n + " premiers entiers est : " + somme);

        // 2. Algorithme de Vérification de Palindrome
        String mot = "radar";
        if (estPalindrome(mot)) {
            System.out.println(mot + " est un palindrome.");
        } else {
            System.out.println(mot + " n'est pas un palindrome.");
        }

        // 3. Algorithme de Tri de Tableau (Tri par Bulles)
        int[] tableau = {5, 2, 9, 1, 5, 6};
        trierTableau(tableau);
        System.out.println("Tableau trié : " + Arrays.toString(tableau));
    }

    // Algorithme 1 : Somme des N premiers entiers
    static int calculerSomme(int n) {
        int somme = 0;
        for (int i = 1; i <= n; i++) {
            somme += i;
        }
        return somme;
    }

    // Algorithme 2 : Vérification de Palindrome
    static boolean estPalindrome(String mot) {
        String inverse = "";
        for (int i = mot.length() - 1; i >= 0; i--) {
            inverse += mot.charAt(i);
        }
        return mot.equals(inverse);
    }

    // Algorithme 3 : Tri de Tableau (Tri par Bulles)
    static void trierTableau(int[] tableau) {
        int n = tableau.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1 - i; j++) {
                if (tableau[j] > tableau[j + 1]) {
                    // Échange les éléments si ils sont dans le mauvais ordre
                    int temp = tableau[j];
                    tableau[j] = tableau[j + 1];
                    tableau[j + 1] = temp;
                }
            }
        }
    }
}
